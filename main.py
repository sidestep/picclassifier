from PIL import Image
import urllib.request
from transformers import pipeline


classifier = pipeline("image-classification", model="models/roomtype")
def clasify(pics):
    labels = classifier(images=pics, num_workers=min(8, len(pics)))
    labels = [l[0]['label'] if l[0]['score'] >= 0.8 else 'NA' for l in labels]
    return list(zip(pics, labels))

if __name__ == '__main__':
    pics = [
             "https://cf2.bstatic.com/xdata/images/hotel/max1024x768/113730612.jpg?k=5e15b2b62f46a20ede24435e32c0253ddfa545fe9c6d22647b17900e03e6c4b8&o=&hp=1",
             "https://cf2.bstatic.com/xdata/images/hotel/max1024x768/113729981.jpg?k=20bf38785fec5e53e092f573d3f2fe60e74c96190a5a060a864d45d4b173fa75&o=&hp=1",
             "https://cf2.bstatic.com/xdata/images/hotel/max1024x768/113729985.jpg?k=1ef8e4f90cb375b64e602a608ac84affe82f64d485c51364756883bd327d036b&o=&hp=1",
             "https://cf2.bstatic.com/xdata/images/hotel/max1024x768/113729987.jpg?k=9c0539b856658c1f3fd74fd7340211fa1ca8a0dd9f9b589a46bc5346166360da&o=&hp=1",
            ] 
    print(clasify(pics))
